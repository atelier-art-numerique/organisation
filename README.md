Ensemble de documents pédagogiques permettant d'assurer les ateliers de création
numérique.

# Contenu pédagogique
L'informatique (aujourd'hui il semblerait qu'on dise *"le numérique"*), ou
*"computer science"* est une discipline scientifique à part entière.

Pourtant, de nombreux spectacles vivants utilisent les nouvelles
technologies comme outils facilitant la technique et la création.

C'est par l'approche créative que je propose d'aborder les outils et
méthodes (à bases de systèmes et **logiciels Libres**) qui permettront
d'envisager de nouvelles façons de créer : le **son**, la **lumière**, la
**vidéoprojection**.

Riche d'une expérience artistique (*"en vrai"* **[groolot]**,
*"Plébiscite"* **[groolot]**, *"Tchernobyl Tremblements"* **[groolot]**, *"sur le
passage, DEBORD"* **[pièces et main d'œuvre]**, *"un coup de dés, jamais n'abolira le hasard, MALLARMÉ"*
**[pièces et main d'œuvre]**) et d'enseignant, Grégory DAVID est l'auteur des
contenus pédagogiques disponibles ici.